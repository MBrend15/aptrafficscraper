import statistics
import os
import csv
import math

directory = './conn_counts'
diff_col = []

for name in os.listdir(directory):
	file_name = os.path.join(os.path.join(directory,name))
	ctr = 1;
	mid_diff = 0;	
	with open(file_name,'rb') as csvfile:
		reader = csv.reader(csvfile,delimiter=',')
		for row in reader:
		#	if int(row[0]) != 0:
			diff = int(row[0])-int(row[1])
			if ctr%24==1:
				diff_col.append(diff+mid_diff)
			
			elif ctr%24==0:
				diff_col.append(diff)
				mid_diff = diff
				
			else:
				diff_col.append(diff)
			ctr = ctr+1

average = statistics.mean(diff_col)
std = statistics.stdev(diff_col)

first_qtr = average - std
sec_qtr = average + std

print first_qtr
print average
print sec_qtr
print std



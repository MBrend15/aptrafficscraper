import math
import MySQLdb
import sqlite3
import urllib3
import json
from datetime import datetime,timedelta, date
import csv
import os

#global variables
ap_list = []
success_list = []
deauth_list = []
diff_list = []
req = urllib3.PoolManager()
#--------------------------methods----------------

def html_query(ap_name,q_date,q_time):
	
	if q_time == 24:
		#req = urllib3.PoolManager
		query = 'http://10.10.200.151:9200/logstash-*/_search?q=%2B@timestamp%3A['+str(q_date)+'T00:00:00+TO+'+str(q_date)+'T23:59:59]+%2Bsyslog_program%3Astm+%2Bap_name%3A%22'+ap_name+'%22+%2Bwlan_event%3A%22Assoc%20success%22&sort=@timestamp:desc&size=5'
		response = req.request('GET',query)
		response_dict = json.loads(response.data)
		success_list.append(response_dict['hits']['total'])

		query = 'http://10.10.200.151:9200/logstash-*/_search?q=%2B@timestamp%3A['+str(q_date)+'T00:00:00+TO+'+str(q_date)+'T23:59:59]+%2Bsyslog_program%3Astm+%2Bap_name%3A%22'+ap_name+'%22+%2Bwlan_event%3A%22Deauth%20to%20sta%22&sort=@timestamp:desc&size=5'		
		response = req.request('GET',query)
		response_dict = json.loads(response.data)
		deauth_list.append(response_dict['hits']['total'])
	else:		
		#req1 = urllib3.PoolManager
		query = 'http://10.10.200.151:9200/logstash-*/_search?q=%2B@timestamp%3A['+str(q_date)+'T00:00:00+TO+'+str(q_date)+'T'+q_time+']+%2Bsyslog_program%3Astm+%2Bap_name%3A%22'+ap_name+'%22+%2Bwlan_event%3A%22Assoc%20success%22&sort=@timestamp:desc&size=5'
		#print query
		response = req.request('GET',query)
		response_dict = json.loads(response.data)
		success_list.append(response_dict['hits']['total'])

		query = 'http://10.10.200.151:9200/logstash-*/_search?q=%2B@timestamp%3A['+str(q_date)+'T00:00:00+TO+'+str(q_date)+'T'+q_time+']+%2Bsyslog_program%3Astm+%2Bap_name%3A%22'+ap_name+'%22+%2Bwlan_event%3A%22Deauth%20to%20sta%22&sort=@timestamp:desc&size=5'		
		response = req.request('GET',query)
		response_dict = json.loads(response.data)
		deauth_list.append(response_dict['hits']['total'])	


#method to generate a range of dates
def daterange(start_date, end_date):
	for n in range(int ((end_date-start_date).days)):
		yield start_date + timedelta(n)

#-----------------main-----------------------------
startTime = datetime.now().time() 

#connect via sqlite to desired database
conn = sqlite3.connect('lib_update_08Dec.db')

#create cursor from connection
cur = conn.cursor()

#execute query
cur.execute('select _id from ap where fold like "LIB%";')
numOfAPs = 82  # len(cur.fetchall())

#laying the foundation for a date iterator

start_date = date(2016, 10, 11)
end_date = date(2016,10,14)

ctr = 0; 
#for everything returned in query, creata a collection
for row in cur.fetchall():		
	ctr = ctr+1
	#ap_list.append(row[0].strip())
	for single_date in daterange(start_date, end_date):
		print '-------'+single_date.strftime("%Y-%m-%d")+' Access Point '+str(ctr)+' of '+str(numOfAPs)+'---------------' 
		for qtime in range(1,25):
			print str(qtime)+':00:00'
			if qtime < 10:
				qu_time = '0'+str(qtime)+':00:00'
			elif qtime>=10 and qtime<24:
				qu_time = str(qtime)+':00:00'
			#print qu_time
			try:
				html_query(row[0].strip(), single_date.strftime("%Y-%m-%d"), qu_time)					
			except ProtocolError as p_er : 
				print 'Error associated with AP: '+row[0].strip()		
conn.commit()
conn.close()

endTime = datetime.now().time()
print startTime
print endTime

print len(success_list)
print len(deauth_list)

#write contents of colelctions into a csv for future processing. name csv after start and end date and save eit to a special directory to vbe accessible by a future script

fn = './conn_counts/'+start_date.strftime('%Y-%m-%d')+'_'+end_date.strftime('%Y-%m-%d')+'.csv'


with open (fn,'wb') as csvfile:
	test = csv.writer(csvfile,delimiter=',')
	for x in range(0, len(success_list)):
		test.writerow([success_list[x],deauth_list[x]]) 


#req = urllib3.PoolManager()
#response = req.request('GET','http://10.10.200.151:9200/logstash-*/_search?q=%2B@timestamp%3A[2017-01-18T00:00:00+TO+2017-01-18T14:05:30]+%2Bsyslog_program%3Astm+%2Bap_name%3A%22LIB-234CA1195B%22+%2Bwlan_event%3A%22Assoc%20success%22+%2Bap_name%3ALIB&sort=@timestamp:desc&size=5')

#print json.loads(response.data.decode('utf-8'))
#json_dict = json.loads(response.data)

#print json_dict['hits']['hits'][4]['_source']['ap_name']
#print json_dict['hits']['total']








